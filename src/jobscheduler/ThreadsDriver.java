/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobscheduler;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author satyanarayana Madala
 */
public class ThreadsDriver {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, InterruptedException {
        // TODO code application logic here
        Scanner scan=new Scanner(new File("threads.txt"));
        JobScheduler jobs=new JobScheduler();
        while (scan.hasNext()) {
            Job job=new Job(scan.next(), scan.nextInt());
            int noOfJobs=scan.nextInt();
            scan.nextLine();
            for (int i = 0; i < noOfJobs; i++) {
                
                String taskName = scan.next();
                Thread newThread;
                switch (taskName) {
                    case "MatrixElementSum":
                        int[][] newIntArray = new int[3][3];
                        for (int k = 0; k < newIntArray.length; k++) {
                            for (int l = 0; l < newIntArray[k].length; l++) {
                                newIntArray[k][l] = scan.nextInt();
                            }
                        }
                        scan.nextLine();
                        MatrixElementSum matrixElementSum = new MatrixElementSum(newIntArray);
                        newThread = new Thread(matrixElementSum);
                        newThread.setName(taskName);
                        Task newTask = new Task(i, newThread);
                        job.addTask(newTask);
                        break;

                    case "Encryption":
                        Encryption encryption = new Encryption(scan.nextLine().trim());
                        newThread = new Thread(encryption);
                        newThread.setName(taskName);
                        Task encTask = new Task(i, newThread);
                        job.addTask(encTask);
                        break;

                    case "Decryption":
                        Decryption decryption = new Decryption(scan.nextLine().trim());
                        newThread = new Thread(decryption);
                        newThread.setName(taskName);
                        Task decTask = new Task(i, newThread);
                        job.addTask(decTask);
                        break;

                    case "SimpleInterest":
                        SimpleInterest simpleInterest = new SimpleInterest(scan.nextFloat(), scan.nextFloat(), scan.nextFloat());
                        scan.nextLine();
                        newThread = new Thread(simpleInterest);
                        newThread.setName(taskName);
                        Task simIntTask = new Task(i, newThread);
                        job.addTask(simIntTask);
                        break;
                }
            
            }
            jobs.addJob(job);
        }
        System.out.println("Running jobs in sorted order.\n");
        jobs.runJobsInOrder();
    }
    
}
