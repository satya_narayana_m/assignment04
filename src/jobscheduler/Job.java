/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobscheduler;

import java.util.HashMap;

/**
 *
 * @author Satyanarayana Madala
 */
public class Job {

    private String jobName;
    private int jobPrority;
    private HashMap<Integer, Task> tasks;

    public Job(String jobName, int jobPrority) {
        this.jobName = jobName;
        this.jobPrority = jobPrority;
        this.tasks = new HashMap<>();
    }

    public String getJobName() {
        return jobName;
    }

    public int getJobPrority() {
        return jobPrority;
    }

    public HashMap<Integer, Task> getTasks() {
        return tasks;
    }

    public void addTask(Task task) {
        tasks.put(task.getTaskId(), task);
    }
}
