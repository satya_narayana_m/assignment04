/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobscheduler;

import java.util.Comparator;
import java.util.TreeSet;

/**
 *
 * @author satyanarayana Madala
 */
public class JobScheduler {

    private TreeSet<Job> sortedJobs;

    public JobScheduler() {
        this.sortedJobs = new TreeSet<Job>(new Comparator<Job>(){

            @Override
            public int compare(Job o1, Job o2) {
                if(o1.getJobPrority()==o2.getJobPrority()){
                    return o1.getJobName().compareTo(o2.getJobName());
                }else{
                    return o1.getJobPrority()-o2.getJobPrority();
                }
            }
            
        });
    }

    public void addJob(Job job) {
        sortedJobs.add(job);
    }

    public void runJobsInOrder() throws InterruptedException {
        for (Job eachjob : sortedJobs) {
            System.out.println("Running "+eachjob.getJobName() +" with priority "+eachjob.getJobPrority());
            for (int i : eachjob.getTasks().keySet()) {
                eachjob.getTasks().get(i).getThread().start();
                eachjob.getTasks().get(i).getThread().join();
            }
            System.out.println("");
        }
    }

}
