/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobscheduler;

/**
 *
 * @author satyanarayana Madala
 */
public class Decryption implements Runnable {

    private String encryptedMessage;
    private String decryptedMessage;

    public Decryption(String encryptedMessage) {
        this.encryptedMessage = encryptedMessage;
        this.decryptedMessage = "";
    }

    @Override
    public void run() {
        for (int i = 0; i < encryptedMessage.length(); i++) {
            int eachChar=encryptedMessage.charAt(i);
            decryptedMessage+=(char)(eachChar ^ 23);          
        }
        System.out.println(Thread.currentThread().getName()+" is Running");
        System.out.println("Decrypted message of '"+encryptedMessage+"' is "+decryptedMessage);
    }

}
