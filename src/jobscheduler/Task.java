package jobscheduler;

/**
 *
 * @author Satyanarayana Madala
 */
public class Task {

    private int taskId;
    private Thread thread;

    public int getTaskId() {
        return taskId;
    }

    public Thread getThread() {
        return thread;
    }

    public Task(int taskId, Thread thread) {
        this.taskId = taskId;
        this.thread = thread;
    }

}
