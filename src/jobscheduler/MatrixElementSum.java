/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobscheduler;

/**
 *
 * @author satyanarayana Madala
 */
public class MatrixElementSum implements Runnable{
    private int[][] matrix;
private int sum;

    public MatrixElementSum(int[][] matrix) {
        this.matrix = matrix;
        this.sum = 0;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+" is Running");
        for (int[] eachRow : matrix) {
            for (int i = 0; i < eachRow.length; i++) {
                sum += eachRow[i];
                
            }
        }
        System.out.println("The sum of the elements of the matrix is "+sum);
    }

}
