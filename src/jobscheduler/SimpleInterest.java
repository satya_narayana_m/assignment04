/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package jobscheduler;

/**
 *
 * @author satyanarayana Madala
 */
public class SimpleInterest implements Runnable{
    private float amount;
private float years;
private float rateOfInterest;

    public SimpleInterest(float amount, float years, float rateOfInterest) {
        this.amount = amount;
        this.years = years;
        this.rateOfInterest = rateOfInterest;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+" is Running");
        System.out.printf(" The resulting amount in the account id $%2.2f \n", amount*(1 + years*(rateOfInterest/100.0)));
    }


}
