/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobscheduler;

/**
 *
 * @author satyanarayana Madala
 */
public class Encryption implements Runnable{

    private String encryptedMessage;
    private String decryptedMessage;

    public Encryption(String decryptedMessage) {
        this.decryptedMessage = decryptedMessage;
        this.encryptedMessage = "";
    }

    @Override
    public void run() {
         for (int i = 0; i < decryptedMessage.length(); i++) {
            int eachChar=decryptedMessage.charAt(i);
            encryptedMessage+=(char)(eachChar ^ 23);          
        }
        System.out.println(Thread.currentThread().getName()+" is Running");
        System.out.println("Decrypted message of '"+decryptedMessage+"' is "+encryptedMessage);
    }

}
